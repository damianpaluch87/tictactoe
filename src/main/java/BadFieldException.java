package main.java;

public class BadFieldException extends Exception{
    public BadFieldException(String message) {
        super(message);
    }
}
