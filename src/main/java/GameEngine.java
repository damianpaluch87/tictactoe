package main.java;

public class GameEngine {

    public static boolean isGameOver(Board board, int needed, int x, int y, char player) {
        if (isWinningMove(board, needed, x, y, player)) {
            board.printWinningBoard(player);
            System.out.println("Koniec gry, gracz " + player + " wygrał");
            return true;
        } else if (board.isFull()) {
            board.printBoard();
            System.out.println("Koniec gry, remis");
            return true;
        }
        return false;
    }

    public static boolean isWinningMove(Board board, int needed, int x, int y, char player) {
        return (checkAxis(board, needed, x, y, 1, 0, player) ||
                checkAxis(board, needed, x, y, 1, 1, player) ||
                checkAxis(board, needed, x, y, 0, 1, player) ||
                checkAxis(board, needed, x, y, 1, -1, player));
    }

    public static int checkDirection(Board board, int x, int y, int rx, int ry, char player) {
        int count = 0;
        while (board.isPlayerControlled(x, y, player)) {
            ++count;
            x += rx;
            y += ry;
        }
        return count;
    }

    public static boolean checkAxis(Board board, int needed, int x, int y, int rx, int ry, char player) {
        return checkDirection(board, x + rx, y + ry, rx, ry, player)
                + checkDirection(board, x - rx, y - ry, rx * -1, ry * -1, player)
                + 1 >= needed;
    }
}
