package main.java;

public class TickTacToe {

    private Board board;
    private int needed;
    private InputManager inputManager;
    private boolean isOver;

    public TickTacToe() {
        this.inputManager = new InputManager();
    }

    public void starter() {

        int boardSize;

        InputManager inputManager = new InputManager();

        do {
            boardSize = inputManager.readBoardSize();
        } while (boardSize < 3 || boardSize > 50);

        int needed;

        if (boardSize == 3)
            needed = 3;
        else if (boardSize <= 5)
            needed = 4;
        else
            needed = 5;

        this.board = new Board(boardSize);
        this.needed = needed;

        gameLoop();
    }

    public void gameLoop() {
        char player = 'X';
        isOver = false;
        while (!isOver) {
            if (makeMove(player))
                player = (char) ('X' + 'O' - player);
        }
    }

    private boolean makeMove(char player) {

        board.printBoard();

        System.out.println("Ruch gracza " + player);

        int x = inputManager.readColumn();

        int y = inputManager.readRow();

        try {
            board.setField(x, y, player);
        } catch (BadFieldException e) {
            System.out.println(e.getMessage());
            return false;
        }

        isOver = GameEngine.isGameOver(board, needed, x, y, player);

        return true;
    }
}
