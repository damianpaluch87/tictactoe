package main.java;

import java.util.NoSuchElementException;
import java.util.Scanner;

public class InputManager {


    private Scanner scanner = new Scanner(System.in);

    public int readColumn() {
        System.out.println("Podaj numer kolumny");
        int result;
        while ((result = getInt()) < 0)
            System.out.println("Podaj poprawny numer kolumnyy");
        return result;
    }
    public int readRow() {
        System.out.println("Podaj numer wiersza");
        int result;
        while ((result = getInt()) < 0)
            System.out.println("Podaj poprawny numer wiersza");
        return result;
    }

    public int readBoardSize() {
        System.out.println("Podaj rozmiar planszy");
        int result;
        while ((result = getInt()) < 0) {
            System.out.println("Podaj poprawny rozmiar planszy");
        }
        return result;
    }

    private int getInt() {
        try {
            String line = scanner.nextLine();
            int value = Integer.valueOf(line);
            return value;
        } catch (NumberFormatException | NoSuchElementException ex) {
            return -1;
        }
    }
}
