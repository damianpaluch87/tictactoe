package main.java;

public class Board {
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_RESET = "\u001B[0m";
    private char[][] board;
    private int left;

    private void initialize() {
        for (int i = 0; i < board.length; ++i)
            for (int j = 0; j < board.length; ++j)
                this.board[i][j] = '·';
    }

    public Board(int size) {
        this.board = new char[size][size];
        left = size * size;
        initialize();
    }

    public void printBoard() {
        for (int i = 0; i < board.length; ++i) {
            for (char[] chars : board) System.out.print(chars[i]);
            System.out.println();
        }
    }
    public void printWinningBoard(char player)  {
        for (int i = 0; i < board.length; ++i) {
            for (char[] chars : board) {
                if(chars[i] == player){
                    System.out.print(ANSI_GREEN + chars[i] + ANSI_RESET);
                }else{
                    System.out.print(chars[i]);
                }
            }
            System.out.println();
        }
    }

    private boolean outOfBounds(int x, int y) {
        return (x < 1 || x > board.length || y < 1 || y > board.length);
    }

    private boolean isEmpty(int x, int y) {
        return getField(x, y) == '·';
    }

    public boolean isPlayerControlled(int x, int y, char player) {
        return getField(x, y) == player;
    }

    private char getField(int x, int y) {
        if (outOfBounds(x, y))
            return '!';
        else
            return board[x - 1][y - 1];
    }

    public void setField(int x, int y, char player) throws BadFieldException{
        if (outOfBounds(x, y))
            throw new BadFieldException("Pole poza planszą");
        if(!isEmpty(x, y))
            throw new BadFieldException("Pole jest zajęte");

        board[x - 1][y - 1] = player;
        left--;
    }

    public boolean isFull() {
        return left <= 0;
    }

}
