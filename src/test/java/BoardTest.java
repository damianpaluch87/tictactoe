package test.java;

import main.java.BadFieldException;
import main.java.Board;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BoardTest {

    private Board board;

    @BeforeEach
    void initialize() throws BadFieldException {
        board = new Board(2);

        board.setField(1, 1, 'X');
        board.setField(1, 2, 'O');
    }

    @Test
    void shouldReturnFalseWhenEmpty() {
        assertFalse(board.isPlayerControlled(2, 1, 'X'));
    }

    @Test
    void shouldReturnFalseWhenOutOfBounds() {
        assertFalse(board.isPlayerControlled(2, 3, 'X'));
    }
    @Test
    void shouldReturnTrueWhenPlayerControlled() {
        assertTrue(board.isPlayerControlled(1, 1, 'X'));
    }

    @Test
    void shouldReturnFalseWhenControlledByOtherPlayer() {
        assertFalse(board.isPlayerControlled(1, 2, 'X'));
    }

    @Test
    void shouldSetFieldWhenEmpty() throws BadFieldException{
        board.setField(2, 1, 'X');
        assertTrue(board.isPlayerControlled(2, 1, 'X'));
    }

    @Test
    void shouldThrowExceptionWhenOutOfBounds() {
        Assertions.assertThrows(BadFieldException.class, () -> {
           board.setField(3,2, 'X');
        });
    }

    @Test
    void shouldThrowExceptionWhenOpponentControlled() {
        Assertions.assertThrows(BadFieldException.class, () -> {
            board.setField(1,2, 'X');
        });
    }

    @Test
    void shouldThrowExceptionWhenOwn() {
        Assertions.assertThrows(BadFieldException.class, () -> {
            board.setField(1,1, 'X');
        });
    }

    @Test
    void shouldReturnFalseWhenNotFull() throws BadFieldException{
        assertFalse(board.isFull());
        board.setField(2, 2, 'X');
        assertFalse(board.isFull());
    }

    @Test
    void shouldReturnTrueWhenFull() throws BadFieldException{
        board.setField(2, 1, 'X');
        board.setField(2, 2, 'X');
        assertTrue(board.isFull());
    }
}