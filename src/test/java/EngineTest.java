package test.java;

import main.java.BadFieldException;
import main.java.Board;
import main.java.GameEngine;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class EngineTest {

    @Test
    void shouldReturnFalseWhenAxisIsNotWinning() throws BadFieldException {
        Board board = new Board(3);
        int needed = 3;
        board.setField(1, 1, 'X');
        board.setField(2, 1, 'X');
        Assertions.assertFalse(GameEngine.checkAxis(board, needed, 2, 1, 1, 0, 'X'));
    }

    @Test
    void shouldReturnTrueWhenAxisIsWinning() throws BadFieldException {
        Board board = new Board(3);
        int needed = 3;
        board.setField(1, 1, 'X');
        board.setField(2, 1, 'X');
        board.setField(3, 1, 'X');
        Assertions.assertTrue(GameEngine.checkAxis(board, needed, 3, 1, 1, 0, 'X'));
    }

    @Test
    void shouldReturnTrueOnWinningMove() throws BadFieldException {
        Board board = new Board(3);
        int needed = 3;
        board.setField(1, 1, 'X');
        board.setField(2, 1, 'X');
        board.setField(3, 1, 'X');
        Assertions.assertTrue(GameEngine.isGameOver(board, needed, 3, 1, 'X'));
    }

    @Test
    void shouldReturnTrueOnFullBoard() throws BadFieldException {
        Board board = new Board(3);
        int needed = 3;
        board.setField(1, 1, 'X');
        board.setField(2, 1, 'Y');
        board.setField(3, 1, 'X');

        board.setField(1, 2, 'Y');
        board.setField(2, 2, 'X');
        board.setField(3, 2, 'Y');

        board.setField(1, 3, 'Y');
        board.setField(2, 3, 'X');
        board.setField(3, 3, 'Y');

        Assertions.assertTrue(GameEngine.isGameOver(board, needed, 3, 3, 'Y'));
    }

    @Test
    void shouldReturnFalseWhenNotOver() throws BadFieldException {
        Board board = new Board(3);
        int needed = 3;
        board.setField(1, 1, 'X');
        board.setField(2, 1, 'Y');
        board.setField(3, 1, 'X');

        board.setField(1, 2, 'Y');
        board.setField(2, 2, 'X');
        board.setField(3, 2, 'Y');

        board.setField(1, 3, 'Y');
        board.setField(2, 3, 'X');

        Assertions.assertFalse(GameEngine.isGameOver(board, needed, 2, 3, 'X'));
    }
}
